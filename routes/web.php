<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes by soengsouy
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::view('/dashboard', 'dashboard')->middleware('auth')->name('dashboard');
Route::group(['middleware' => 'auth:admin'], function () {
});

Auth::routes();

//============================== Login Socialite ===========================//
// Route::get('/','App\Http\Controllers\Socialite\LoginController@redirectToProvider')->name('login.facebook');
// Route::get('/callback','App\Http\Controllers\Socialite\LoginController@handleProviderCallback');

// -----------------------------user role-----------------------------------------
Route::get('userRole','App\Http\Controllers\HomeController@userRole')->name('userRole');
Route::get('dashboard','App\Http\Controllers\HomeController@index')->name('dashboard');

// ======================== avatar ======================== //
Route::post('profile', 'App\Http\Controllers\HomeController@update_avatar')->name('profile');

// -----------------------------login-----------------------------------------
Route::get('/login', 'App\Http\Controllers\Auth\LoginController@login')->name('login');
Route::post('/login', 'App\Http\Controllers\Auth\LoginController@authenticate');
Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

// ------------------------------register---------------------------------------
Route::get('/register', 'App\Http\Controllers\Auth\RegisterController@register')->name('register');
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@storeUser')->name('register');

// -----------------------------forget password ------------------------------
Route::get('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@getEmail')->name('forget-password');
Route::post('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@postEmail')->name('forget-password');

Route::get('reset-password/{token}', 'App\Http\Controllers\Auth\ResetPasswordController@getPassword');
Route::post('reset-password', 'App\Http\Controllers\Auth\ResetPasswordController@updatePassword');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/user-profile', [App\Http\Controllers\HomeController::class, 'viewProfile'])->name('user-profile');

//============================== route student =============================//
// ----------------------------- view student ------------------------------
Route::get('form/student/new', [App\Http\Controllers\StudentController::class, 'viewStudent'])->name('form/student/new');
// -----------------------------save information student ------------------------------
Route::post('form/student/save', [App\Http\Controllers\StudentController::class, 'store'])->name('form/student/save');
// -----------------------------report student ------------------------------
Route::get('report/student/view', [App\Http\Controllers\StudentController::class, 'reportStudent'])->name('report/student/view');
// -----------------------------view detail student ------------------------------
Route::get('report/student/view/detail/{id}', [App\Http\Controllers\StudentController::class, 'viewDetail'])->name('report/student/view/detail');
// -----------------------------edit student ------------------------------
Route::get('report/student/view/edit/{id}', [App\Http\Controllers\StudentController::class, 'editStudent'])->name('report/student/view/edit');
// -----------------------------update student ------------------------------
Route::post('form/student/update', [App\Http\Controllers\StudentController::class, 'updateStudent'])->name('form/student/update');


//============================== route techer =============================//
// ----------------------------- view techer ------------------------------
Route::get('form/techer/new', [App\Http\Controllers\TecherController::class, 'viewTecher'])->name('form/techer/new');

//============================== route maintainane =============================//
// ----------------------------- view user role ------------------------------
Route::get('group/user/role/mng', [App\Http\Controllers\UserManagementController::class, 'viewUserRole'])->name('group/user/role/mng');
// ----------------------------- view group user ------------------------------
Route::get('group/user/mng', [App\Http\Controllers\UserManagementController::class, 'viewUserGroup'])->name('group/user/mng');