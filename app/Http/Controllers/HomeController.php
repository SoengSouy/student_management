<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user      =   DB::table('users')->count();
        // $teacher   =   DB::table('tbl_teacher')->count();
        $student   =   DB::table('tbl_student')->count();

        return view('home',compact('user','student'));
    }

    // view profile
    public function viewProfile()
    {
        return view('user-profile');
    }
}
