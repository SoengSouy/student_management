# admin_student
-------------------
Administrator
-------------
Manage students class/group 
Add / edit / delete student
Add / edit / delete student final marks
View profile of students
Create / edit / delete scholarship for students
Manage teacher profile
Add / edit / delete teacher information
----------------------------------------------
git clone https://gitlab.com/SoengSouy/student_management.git
cd laravel-school-management-system
composer install
npm install
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan serve