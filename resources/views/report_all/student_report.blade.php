@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.report_all')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Form / Student / New</h5>
                            <p class="text-muted m-b-10">for input information</p>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">student</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
        
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Infomation Input</h4>
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Sex</th>
                                                    <th>Email</th>
                                                    <th>Phone Number</th>
                                                    <th>Age</th>
                                                    <th>Province</th>
                                                    <th>District</th>
                                                    <th>Subject</th>
                                                    <th>School Year</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($tbl_student as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->full_name }}</td>
                                                    <td>{{ $item->sex }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>{{ $item->phone_number }}</td>
                                                    <td>{{ $item->age }}</td>
                                                    <td>{{ $item->province }}</td>
                                                    <td>{{ $item->district }}</td>
                                                    <td>{{ $item->subject }}</td>
                                                    <td>{{ $item->school_year }}</td>
                                                    <td>
                                                        <a href="{{ route('report/student/view/detail',['id'=>$item->id]) }}"><i class="fa fa-eye" style="color:rgb(14, 151, 87);font-size:20px"></i></a>
                                                        <a href="{{ route('report/student/view/edit',['id'=>$item->id]) }}"><i class="fa fa-edit" style="color:rgb(3, 217, 255);font-size:20px"></i></a>
                                                        <a href="#"><i class="fa fa-trash" style="color:red;font-size:20px"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>                          
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
