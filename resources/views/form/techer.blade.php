@extends('layouts.master')
@section('contain')
<div class="pcoded-wrapper">
    @include('sidebar.techer')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header card">
                        <div class="card-block">
                            <h5 class="m-b-10">Form / Techer / New</h5>
                            <p class="text-muted m-b-10">for input information</p>
                            <ul class="breadcrumb-title b-t-default p-t-10">
                                <li class="breadcrumb-item">
                                    <a href="{{ route('home') }}"> <i class="fa fa-home"></i> </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">form</a></li>
                                <li class="breadcrumb-item"><a href="#!">Techer</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page-header end -->
         
                    <!-- Page body start -->
                    <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-block">
                                        <h4 class="sub-title">Infomation Input</h4>
                                        
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                                        @endif

                                        <form id="validate" name="form" action="{{ route('form/student/save') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Full Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Enter name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Sex</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="sex" name="sex">
                                                        <option selected disabled>Please select sex</option>
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control text-danger" id="email" name="email" placeholder="Enter email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Phone number</label>
                                                <div class="col-sm-10">
                                                    <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Enter phone number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Age</label>
                                                <div class="col-sm-10">
                                                    <input type="number" class="form-control" id="age" name="age" placeholder="Enter age">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Country</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="country" name="country">
                                                        <option selected disabled>Please select Country</option>
                                                        <option value="Cambodia">Cambodia</option>
                                                        <option value="Thailand">Thailand</option>
                                                        <option value="Vietnam">Vietnam</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Province</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="province" name="province" placeholder="Enter province">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">District</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="district" name="district" placeholder="Enter district">
                                                </div>
                                            </div>
                                        
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Subject</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="subject">
                                                        <option selected disabled>Please select Subject</option>
                                                        <option value="IT Infomation">IT Infomation</option>
                                                        <option value="Accounting">Accounting</option>
                                                        <option value="Khmer language">Khmer language</option>
                                                        <option value="English language">English language</option>
                                                        <option value="Thailand language">Thailand language</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">School year</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="schoolYear">
                                                        <option selected disabled>Please select year</option>
                                                        <option value="year 1">year 1</option>
                                                        <option value="year 2">year 2</option>
                                                        <option value="year 3">year 3</option>
                                                        <option value="year 4">year 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                               
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-success"><i class="icofont icofont-check-circled"></i>Save</button>
                                                    <button type= "reset"class="btn btn-danger"><i class="icofont icofont-warning-alt"></i>Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $("form#validate").validate({
        rules: {
            fullName: {
                    required: true,
                },
            sex:{
                required: true,
            },
            email:{
                required: true,
            },
            phoneNumber:{
                required: true,
            },
            age:{
                required: true,
            },
            country:{
                required: true,
            }, 
            province:{
                required: true,
            }, 
            district:{
                required: true,
            },
            subject:{
                required: true,
            }, 
            schoolYear:{
                required: true,
            },    
                    
        },
        messages: {
            fullName: {
                    required: "Please enter full name",
                },
            sex:{
                required: "Please select sex",
            },
            email:{
                required: "Please enter email",
            },
            phoneNumber:{
                required: "Please enter phone number",
            },
            age:{
                required: "Please enter age",
            },
            country:{
                required: "Please select country",
            },
            province:{
                required: "Please enter province",
            },
            district:{
                required: "Please enter district",
            },
            subject:{
                required: "Please select subject",
            },
            schoolYear:{
                required: "Please enter school year",
            },           
        },
        
    });
    </script>
    
@endsection
